> #### _kódmágia_

# XSD készítés

Készítsen egy XSD-t kerékpárok adatainak tárolásához.

A következő adatokat kell tárolnunk:

- kerék átmérője
- vázszám
- szín
- típus
- vásárlás dátuma
 - év
 - hó
 - nap
- tulajdonos

__Segítség__

[Előadás](../materials/definitions1.md)

> #### _kódmágia_
